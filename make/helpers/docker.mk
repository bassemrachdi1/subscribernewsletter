####################
##     Docker     ##
####################

.PHONY: docker@configure docker@build docker@up docker@down docker@rebuild-base

## Copy the docker-compose.override.yml.dist on docker-compose.override.yml if not exist
docker@configure:
	cp -n docker-compose.override.yml.dist docker-compose.override.yml || true

## Build the docker infrastructure
docker@build:
	$(DOCKER_COMPOSE) pull --ignore-pull-failures
	$(DOCKER_COMPOSE) build --force-rm

## Run the docker infrastructure in detached mode
docker@up:
	$(DOCKER_COMPOSE) up -d

## Stop and remove the docker infrastructure
docker@down:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) rm -v --force

## Rebuild and push containers from docker-compose.base.yml
docker@rebuild-base:
	$(DOCKER_COMPOSE) -f docker-compose.base.yml build
	$(DOCKER_COMPOSE) -f docker-compose.base.yml push
