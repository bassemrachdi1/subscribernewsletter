project@start: project@up project@db

###################
##    Project    ##
###################

## Install and start the project
project@up:
	composer install
	docker-compose up -d

project@db: project@db-create project@db-diff project@db-migrate project@db-fixture

project@db-drop:
	docker exec -it newsletter-php-fpm bin/console doctrine:database:drop --force

project@db-create:
	docker exec -it newsletter-php-fpm bin/console doctrine:database:create

project@db-diff:
	docker exec -it newsletter-php-fpm bin/console doctrine:migration:diff

project@db-migrate:
	docker exec -it newsletter-php-fpm bin/console doctrine:migration:migrate

project@db-fixture:
	docker exec -it newsletter-php-fpm bin/console doctrine:fixtures:load

project@send-newsletter:
	docker exec -it newsletter-php-fpm bin/console app:send-newsletter

