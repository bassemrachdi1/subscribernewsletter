# Newsletter

## Exercice:
    la creation d'un systeme d'abonnement de newsletter 


# Lacement du projet

  - Pour lancer le projet vous allez voir que dans le Makefile j'ai mis quelque commande afin de faciliter le lancement du project:

  - git clone https://gitlab.com/bassemrachdi1/subscribernewsletter.git

# Commande a suivre par étape

  - composer install
  - docker-compose up -d
  - docker exec -it newsletter-php-fpm bin/console doctrine:database:drop --force
  - docker exec -it newsletter-php-fpm bin/console doctrine:database:create
  -  docker exec -it newsletter-php-fpm bin/console doctrine:migration:diff
  -  docker exec -it newsletter-php-fpm bin/console doctrine:migration:migrate
  -  docker exec -it newsletter-php-fpm bin/console doctrine:fixtures:load
  -  


# Lien utile 
    project :   http://localhost:8000/
    mailhog: http://localhost:8025/
    rabbitmq: http://localhost:15672/ ( username: rabbit , password: mq)
    
# commande d'envoie des messages par lot:
    docker exec -it newsletter-php-fpm bin/console app:send-newsletter
