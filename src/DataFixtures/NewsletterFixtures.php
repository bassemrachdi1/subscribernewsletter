<?php

namespace App\DataFixtures;

use App\Entity\Newsletter;
use App\Entity\NewsletterInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class NewsletterFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {

            /** @var NewsletterInterface $newsletter */
            $newsletter = new Newsletter();
            $newsletter->setName('newsletter '.$i);
            $manager->persist($newsletter);
        }
        $manager->flush();
    }
}
