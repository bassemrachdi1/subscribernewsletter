<?php

namespace App\Consumer;

use App\Entity\SubscriberInterface;
use App\Mailer\NewsletterMailer;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmailConsumer
{
    /** @var ContainerInterface $container */
    protected $container;
    /**
     * @var NewsletterMailer
     */
    private $newsletterMailer;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, NewsletterMailer $newsletterMailer)
    {
        $this->container = $container;
        $this->newsletterMailer = $newsletterMailer;
    }


    public function execute($subscriber, $newsletter)
    {
        return $this->processMessage($subscriber, $newsletter);
    }


    public function processMessage($subscriber, $newsletter)
    {
        try{
            $this->newsletterMailer->sendNewsletterEmail( $subscriber, $newsletter);
        } catch (\Exception $exception) {
            return ConsumerInterface::MSG_SINGLE_NACK_REQUEUE;
        }

        return ConsumerInterface::MSG_ACK;
    }
}
