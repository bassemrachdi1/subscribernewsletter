<?php

namespace App\Command;

use App\Consumer\EmailConsumer;
use App\Entity\NewsletterInterface;
use App\Mailer\NewsletterMailer;
use App\Repository\NewsletterRepository;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\Mailer;

class SendingNewsletterCommand extends Command
{
    protected static $defaultName = 'app:send-newsletter';
    /**
     * @var null
     */
    private $name;
    /**
     * @var NewsletterRepository
     */
    private $newsletterRepository;

    /**
     * @var EmailConsumer
     */
    private $consumer;

    public function __construct($name = null, NewsletterRepository $newsletterRepository, NewsletterMailer $mailer, EmailConsumer $consumer)
    {
        parent::__construct($name);
        $this->newsletterRepository = $newsletterRepository;
        $this->consumer = $consumer;
    }

    protected function configure()
    {
        $this
            ->setName('newsletter:send')
            ->setDescription('Sending newsletters in batch mode')
            ->setHelp('This command allow you to send newsletters in batch mode')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var NewsletterInterface $newsletter */
        $newsletter = $this->newsletterRepository->findOneBy(['name' => 'newsletter 0']);

        $subscribers = $newsletter->getSubscribers();

        foreach ($subscribers as $subscriber)
        {
            $this->consumer->execute($subscriber, $newsletter);
        }
        $output->writeln($this->getHelper('formatter')->formatSection(
            'success',
            sprintf(
                'Successfully handled %d %s newsletter(s)',
                count($subscribers),
                'newsletter'
            )));
    }
}
