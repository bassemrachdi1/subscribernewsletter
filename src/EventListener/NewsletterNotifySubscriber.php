<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Event\Events;
use App\Mailer\NewsletterMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Envoi un mail de bienvenue à chaque abonnement
 *
 */
class NewsletterNotifySubscriber implements EventSubscriberInterface
{
    private $mailer;

    public function __construct(NewsletterMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::USER_SUBSCRIPTION => 'onUserSubscription',
        ];
    }

    public function onUserSubscription(GenericEvent $event): void
    {
        $arguments = $event->getArguments();

        $this->mailer->sendSubscriptionConfirmationEmail($arguments);
    }
}
