<?php

namespace App\Entity;
use Doctrine\Common\Collections\Collection;

interface SubscriberInterface
{
    public function getId(): ?int;

    public function getName(): ?string;

    public function setName(?string $name): void;

    public function getEmail(): ?string;

    public function setEmail(string $email): void;

    /**
     * @return Collection|Newsletter[]
     */
    public function getNewsletters(): Collection;

    public function addNewsletter(NewsletterInterface $newsletter): void;

    public function removeNewsletter(NewsletterInterface $newsletter): void;
}
