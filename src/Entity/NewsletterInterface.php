<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 15/12/19
 * Time: 14:02
 */

namespace App\Entity;


use Doctrine\Common\Collections\Collection;

interface NewsletterInterface
{
    public function getId(): ?int;

    public function getName(): ?string;

    public function setName(string $name): void;

    public function getSubscribers(): Collection;

    public function addSubscriber(SubscriberInterface $subscriber): void;

    public function removeSubscriber(SubscriberInterface $subscriber): void;
}
