<?php

namespace App\Controller;

use App\Entity\Newsletter;
use App\Entity\NewsletterInterface;
use App\Entity\Subscriber;
use App\Entity\SubscriberInterface;
use App\Event\Events;
use App\Form\SubscriberType;
use App\Repository\NewsletterRepository;
use App\Repository\SubscriberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class NewsletterController extends AbstractController
{
    /**
     * @var NewsletterRepository
     */
    private $newsletterRepository;
    /**
     * @var SubscriberRepository
     */
    private $subscriberRepository;

    public function __construct(NewsletterRepository $newsletterRepository, SubscriberRepository $subscriberRepository)
    {
        $this->newsletterRepository = $newsletterRepository;
        $this->subscriberRepository = $subscriberRepository;
    }

    /**
     * @Route("/", name="newsletter")
     */
    public function indexAction()
    {
        $newsletters = $this->newsletterRepository->findAll();

        return $this->render('newsletter/index.html.twig', [
            'newsletters' => $newsletters
        ]);
    }

    /**
     * @Route("/newsletter/{name}", name="newsletter_show")
     */
    public function showAction(Request $request, string $name, EventDispatcherInterface $eventDispatcher)
    {

        /** @var NewsletterInterface $newsletter */
        $newsletter =$this->newsletterRepository->findOneBy(['name' => $name]);
        $subscriber = new Subscriber();

        $form = $this->createForm(SubscriberType::class, $subscriber);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->addFlash('success', 'good!');

            $subscriber = $form->getData();

            $subscriber->addNewsletter($newsletter);
            $this->persist($subscriber);
            $newsletter->addSubscriber($subscriber);
            $this->persist($newsletter);


            $event = new GenericEvent(null, ['newsletter' => $newsletter, 'subscriber' => $subscriber]);
            $eventDispatcher->dispatch(Events::USER_SUBSCRIPTION, $event);

            return $this->redirectToRoute('newsletter');
        }

        return $this->render('newsletter/show.html.twig', [
            'form' => $form->createView(),
            'newsletter' => $newsletter,
        ]);
    }

    /**
     * @Route("/newsletter/delete/{newsletterId}/{subscriberId}", name="delete_subscriber")
     */
    public function deleteAction($newsletterId, $subscriberId)
    {
        $newsletter = $this->newsletterRepository->find($newsletterId);

        if (!$newsletter) {
            throw $this->createNotFoundException('No guest found for id '.$newsletterId);
        }

        /** @var SubscriberInterface $subscriber */
        $subscriber = $this->subscriberRepository->find($subscriberId);
        $newsletter->removeSubscriber($subscriber);
        $this->persist($newsletter);

        return $this->redirectToRoute('newsletter');
    }

    private function persist($resource)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($resource);
        $entityManager->flush();
    }
}
