<?php

declare(strict_types=1);

namespace App\Mailer;

use App\Entity\NewsletterInterface;
use App\Entity\NewsletterSubscription;
use App\Entity\Subscriber;
use App\Entity\SubscriberInterface;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Templating\EngineInterface;
use Twig\Environment;

class NewsletterMailer
{
    const FROM_EMAIL='bassemrachdi1@gmail.com';
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;
    /**
     * @var UrlGeneratorInterface
     */
    protected $router;
    /**
     * @var Environment
     */
    private $twig;

    /**
     * Mailer constructor.
     *
     * @param \Swift_Mailer         $mailer
     * @param UrlGeneratorInterface $router
     * @param Environment $twig
     */
    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface  $router, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
    }

    public function sendSubscriptionConfirmationEmail($arguments)
    {


        $rendered = $this->twig->render('Mail/subscription.html.twig', [
            'newsletter_name' => $arguments['newsletter']->getName(),
            'subscriber_name' => $arguments['subscriber']->getName(),
        ]);

        $this->sendEmail($rendered, self::FROM_EMAIL, (string) $arguments['subscriber']->getEmail());
    }

    /**
     * @param SubscriberInterface $subscriber
     * @param NewsletterInterface $newsletter
     */
    public function sendNewsletterEmail($subscriber, $newsletter)
    {

        $deleteUrl = $this->router->generate('delete_subscriber', ['newsletterId' => $newsletter->getId(), 'subscriberId' => $subscriber->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        $rendered = $this->twig->render('Mail/newsletter.html.twig', array(
            'newsletter' => $newsletter,
            'subscriber' => $subscriber,
            'delete_url' => $deleteUrl,
        ));

        $this->sendEmail($rendered, self::FROM_EMAIL, (string) $subscriber->getEmail());
    }

    /**
     * @param string       $renderedTemplate
     * @param array|string $fromEmail
     * @param array|string $toEmail
     */
    protected function sendEmail($template, $sender, $recipient)
    {
        $renderedLines = explode("\n", trim($template));
        $subject = array_shift($renderedLines);
        $body = implode("\n", $renderedLines);
        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setTo($recipient)
            ->setFrom($sender)
            ->setBody($body, 'text/html')
        ;
        $this->mailer->send($message);
    }
}
